import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

// Services
import {CompanyDataService} from '../model/company-data.service'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  formGroup:FormGroup;
  results:any;
  loading=false;


  constructor(
    private service:CompanyDataService
  ) {}

  ngOnInit() {
    this.formGroup = new FormGroup({
      q: new FormControl(''),
    });
  }

  submit(data) {
    this.loading = true;
    this.formGroup.controls['q'].disable();
    this.service.getSearch(data).subscribe((result:any) => {

      this.results = result.rows;
      console.log(this.results);
      this.loading = false;
      this.formGroup.controls['q'].enable();
    })
  }
}
