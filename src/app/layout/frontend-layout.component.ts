import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend-layout.component.html'
})
export class FrontendLayoutComponent implements OnInit, AfterViewChecked {
  public today: Date;

  constructor( private cdRef: ChangeDetectorRef) {
    this.today = new Date();
  }

  ngOnInit(){}

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }


}
