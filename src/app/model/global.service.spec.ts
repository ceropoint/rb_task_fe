import { GlobalService } from './global.service';
import { environment } from '../../environments/environment';

describe('global.service.ts', () => {
  let globalService: GlobalService;
  let spy: any;

  beforeEach(() => {
    globalService = new GlobalService();
  });

  describe('constructor()', () => {
    it('should set apiHost same as environment value', () => {
      expect(globalService.apiHost).toEqual(environment.apiHost);
    });
  });

  describe('GlobalService.handleError()', () => {});
});
