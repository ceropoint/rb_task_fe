import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { GlobalService } from './global.service';
import { Company } from './company';
import { ResponseBody } from './response-body';

@Injectable()
export class CompanyDataService {
  constructor(private globalService: GlobalService,  private http: HttpClient) {}

  // GET /v1/company/search
  getSearch(extendedQueries?: any): Observable<Company> {
    const headers = this.getHeaders();

    let queries = {};
        if (extendedQueries) {
            queries = Object.assign(queries, extendedQueries);
        }

    return this.http
      .get<ResponseBody>(this.globalService.apiHost + '/company?'
      + GlobalService.serializeQueryString(queries), {
        headers
      })
      .pipe(
        map(response => {
          return response.data as Company;
        }),
        catchError(err => this.handleError(err))
      );
  }



  private getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  private handleError(response: any) {
    let errorMessage: any = {};
    // Connection error
    if (response.error.status === 0) {
      errorMessage = {
        success: false,
        status: 0,
        data: 'Sorry, there was a connection error occurred. Please try again.'
      };
    } else {
      errorMessage = response.error;
    }

    return throwError(errorMessage);
  }
}
