import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable()
export class GlobalService {
  public apiHost: string;

  public setting: any = {};

  constructor() {
    this.apiHost = environment.apiHost;
  }

  /**
     * Serializes the form element so it can be passed to the back end through the url.
     * The objects properties are the keys and the objects values are the values.
     * ex: { "a":1, "b":2, "c":3 } would look like ?a=1&b=2&c=3
     *
     * @param obj
     * @returns string
     */
    public static serializeQueryString(obj: any): string {
      return Object.keys(obj).map(k => `${k}=${encodeURIComponent(obj[k])}`).join('&');
  }

  static handleError(response: any) {
    let errorMessage: any = {};
    // Connection error
    if (response && response.error && response.error.status === 0) {
      errorMessage = {
        success: false,
        status: 0,
        data: 'Sorry, there was a connection error occurred. Please try again.'
      };
    } else {
      errorMessage = response.error || 'Unknown error';
    }

    return throwError(errorMessage);
  }

  
}
