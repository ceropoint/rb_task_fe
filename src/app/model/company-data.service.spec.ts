import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse  } from '@angular/common/http';

import { CompanyDataService } from './company-data.service';
import { GlobalService } from './global.service';
import {of} from 'rxjs'; // Add import
import { Company } from './company';

describe('company-data.service.ts', () => {
    let service: CompanyDataService;
    let global:GlobalService;

    let httpClientSpy: { get: jasmine.Spy };
    let expectedComp: any;
    let expectedComp2:any;
    

    beforeEach(async(() => {


      httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
      global = new GlobalService();
      service = new CompanyDataService(global,httpClientSpy as any);
     
    }));



    it('should return expected companies by calling once', () => {
      expectedComp = [
        {
          id: 1,
          company_name: "Test Name 1",
          company_number: "555555",
          address_line_1: 'Lorem ipsum',
          address_line_2: 'Dolor sit',
          post_town:'Varna',
          country:'BG',
          post_code:'9000'
        },
        {
            id: 2,
            company_name: "Test Name 2",
            company_number: "555555",
            address_line_1: 'Lorem ipsum',
            address_line_2: 'Dolor sit',
            post_town:'Varna',
            country:'BG',
            post_code:'9000'
        },
      ];

      httpClientSpy.get.and.returnValue(of(expectedComp));

      
      service.getSearch({q:'Cera'}).subscribe(
        response => {
          expect(response).toBeDefined();

          expect(response).toEqual(expectedComp, 'should return expected companies')
        } 
      );
      

      
    });

 
    it('should be OK returning no company', () => {
      expectedComp2 = [{}];
      httpClientSpy.get.and.returnValue(of(expectedComp2));

      service.getSearch().subscribe(
        response => {
          console.log(response);
          expect(response).toBeDefined();

          expect(response).toEqual(expectedComp2, 'should have empty company array')
        }
        
      );


    });

    it('should return an error when the server returns a 404', () => {
      const errorResponse = new HttpErrorResponse({
        error: 'test 404 error',
        status: 404, statusText: 'Not Found'
      });

      console.log(errorResponse);
    
      httpClientSpy.get.and.returnValue(of(errorResponse));
    
      service.getSearch().subscribe(
        response => {fail('expected an error, not results')},
        error  => expect(error.message).toContain('test 404 error')
      );
      });

  });