// tslint:disable: variable-name

export class Company {
  id: number;
  company_name: string;
  company_number: string;
  address_line_1: string;
  address_line_2: string;
  post_town: string;
  country: string;
  post_code: string;

  constructor(values: object = {}) {
    Object.assign(this, values);
  }
}
