import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FrontendLayoutComponent } from './layout/frontend-layout.component';
import { P404Component } from './page/404.component';

export const routes: Routes = [
  {
    path: '',
    component: FrontendLayoutComponent,
    pathMatch: 'full',
    loadChildren: 'app/index/index.module#IndexModule'
  },
  // otherwise redirect to home
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
